import express from 'express';
import fallback from 'express-history-api-fallback';
import proxy from 'express-http-proxy';
import compression from 'compression';

const options = {
  etag: true,
  index: true,
  extensions: ['html'],
  maxAge:'31536000'
}

const PORT = process.env.PORT || 8080
const app = express(options);

app.use(compression({level: 9, memLevel: 9}));
app.use(express.static('dist'));

app.use('/api', proxy('https://tv-v2.api-fetch.website'))

app.use(fallback('dist/index.html',{root:__dirname}))

app.listen(PORT, ()=>{
  console.log(`running on ${PORT}`);
})