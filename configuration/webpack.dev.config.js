var webpack = require('webpack');
var path = require('path');
//important plugin to not have to manage the html view
var HtmlWebpackPlugin = require('html-webpack-plugin');
//dist file to provide our static assets important when dealing with isomorphic
const BUILD_DIR = path.resolve(__dirname, '../dist');
const APP_DIR = path.resolve(__dirname, '../src');
//documented issue https://github.com/webpack/webpack-dev-server/issues/720 when adding additional extensions this will break
const BASE_EXTENSIONS = ['.js'];


// 'css-loader?modules&importLoaders=1&localIndetName=[path]__[name]__[local]__[hash:base64:5]',
var configuration = {
  entry: [
    "babel-polyfill",
    "react-hot-loader/patch",
    `${APP_DIR}/index.jsx`,
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: BUILD_DIR,
    host: '0.0.0.0',
    compress: true,
    port: 8084,
    open: true,
    proxy: {
      "/api":"http://45.55.85.46"
    }
  },
  output: {
    path: BUILD_DIR,
    filename: '[name]_bundle.js',
    publicPath: '/'
  },
  module: {
    loaders: [
      {
        test: /\.json+/,
        loader: ['json-loader']
      },
      {
        test: /\.js[x]+/,
        loader: ['babel-loader']
      },
      {
        test: /\.(png)/,
        loader: ['url-loader']
      },
      {
        test: /\.css$/,
        loaders: [
          'style-loader?sourceMap&hmr',
          'css-loader?modules&importLoaders=1&localIndetName=[name]',
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: path.resolve(__dirname, './postcss.config.js')
              }
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: [...BASE_EXTENSIONS, ...['.jsx', '.css', '.json']],
  },
  devtool: 'eval-source-map',
  plugins: [new HtmlWebpackPlugin(
    {
      title: 'Code Corn Time',
      template: 'src/index.html',
    })
    ,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ],
}

module.exports = configuration;
