var webpack = require('webpack');
var path = require('path');
//important plugin to not have to manage the html view
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
//dist file to provide our static assets important when dealing with isomorphic
const BUILD_DIR = path.resolve(__dirname, '../dist');
const APP_DIR = path.resolve(__dirname, '../src');
//documented issue https://github.com/webpack/webpack-dev-server/issues/720 when adding additional extensions this will break
const BASE_EXTENSIONS = ['.js'];


// 'css-loader?modules&importLoaders=1&localIndetName=[path]__[name]__[local]__[hash:base64:5]',
var configuration = {
  entry: [
    `${APP_DIR}/index.jsx`,
  ],
  output: {
    path: BUILD_DIR,
    filename: '[name]_[hash]_app.js',
    publicPath: "/"
  },
  module: {
    loaders: [
      {
        test: /\.json+/,
        loader: ['json-loader']
      },
      {
        test: /\.js[x]+/,
        loader: ['babel-loader']
      },
      {
        test: /\.(png)/,
        loader: ['url-loader']
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader?modules&importLoaders=1&localIndetName="[name][hash:base64:5]"&minimize',
            {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: path.resolve(__dirname, './postcss.config.js')
                }
              }
            }
          ]
        })
      }
    ]
  },
  resolve: {
    extensions: [...BASE_EXTENSIONS, ...['.jsx', '.css', '.json']],
  },
  devtool: false,
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
       name: "commons",
       filename: "commons.js",
       minChunks: 2,
       deepChildren: true
    }),
    new HtmlWebpackPlugin(
      {
        minifiy: true,
        inject: true,
        title: 'Code Corn Time',
        template: 'src/index.html'
      }),
    // new webpack.optimize.UglifyJsPlugin(),
    new UglifyJSPlugin(),
    new ExtractTextPlugin({filename: '[name]_[hash]_app.css', allChunks: true, ignoreOrder: true}),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production')
    }),
  ],
}

module.exports = configuration;
