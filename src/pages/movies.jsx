import React from 'react';
import {connect} from 'react-redux';
import MainContainer from 'code_corn_time_components/src/main/main-container';
import Menu from '../components/menu/menu';
import Container from '../components/poster/container';

const mapStateToProps = ()=>{
  return {};
}

const mapDispatchToProps = ()=>{
  return {};
}

const Movies = () => {
  return (
    <MainContainer {...{items:[<Menu/>,<Container/>]}} />
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
