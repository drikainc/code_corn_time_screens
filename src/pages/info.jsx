import React from 'react';
import {connect} from 'react-redux';
import Menu from '../components/menu/menu';
import MainContainer from 'code_corn_time_components/src/main/main-container';

const mapStateToProps = ()=>{
  return {};
}

const mapDispatchToProps = ()=>{
  return {};
}

const Detail = () => {
  return (
    <MainContainer {...{items:[<Menu />]}} />
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
