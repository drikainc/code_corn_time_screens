import React from 'react';
import {connect} from 'react-redux';
import MainContainer from 'code_corn_time_components/src/main/main-container';
import PlayerContainer from 'code_corn_time_components/src/components/detail/player_container';

const mapStateToProps = ({detail})=>{
  const {content} = detail;
  return Object.assign({},content, detail,{id:content.imdb_id});
}

const mapDispatchToProps = (dispatch)=>{
  return {
  };
}

const DetailContent = ({images={}, id, title})=>{
  const backgroundImage = images.fanart;
  return (
    <PlayerContainer {...{
          imgSrc: backgroundImage,
          id,
          title,
        }}
    />
  );
}
const DetailContentDecorated = connect(mapStateToProps, mapDispatchToProps)(DetailContent)

const Detail = () => {
  return (
    <MainContainer {...{items:[<DetailContentDecorated />]}} />
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
