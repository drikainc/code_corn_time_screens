import React from 'react';
import {connect} from 'react-redux';
import {goBack, push} from 'react-router-redux';
import Menu from '../components/menu/menu';
import MainContainer from 'code_corn_time_components/src/main/main-container';
import DetailContainer from 'code_corn_time_components/src/components/detail/container';
import DetailPoster from 'code_corn_time_components/src/components/detail/poster';
import {
  focusBookmarkButtonActionCreator,
  focusSeenButtonActionCreator,
  focusWatchButtonActionCreator,
  focusTrailerButtonActionCreator,
  bookmarkDetailSelectActionCreator,
  seenDetailSelectActionCreator,
} from '../actions/detail';

const mapStateToProps = ({detail,films})=>{
  const {content} = detail;
  const {bookmarked} = films;
  console.log(content, bookmarked);
  return Object.assign({},content, {id:content.imdb_id});
}

const mapDispatchToProps = (dispatch)=>{
  return {
    onBackClick:()=>{
      dispatch(goBack())
    },
    onBookmarkButtonEnter: ()=>{
      dispatch(focusBookmarkButtonActionCreator());
    },
    onBookmarkButtonLeave: ()=>{
      dispatch(focusBookmarkButtonActionCreator());
    },
    onBookmarkButtonClick: (id)=>{
      dispatch(bookmarkDetailSelectActionCreator(id));
    },
    onSeenButtonEnter: ()=>{
      dispatch(focusSeenButtonActionCreator());
    },
    onSeenButtonLeave: ()=>{
      dispatch(focusSeenButtonActionCreator());
    },
    onSeenButtonClick: (id)=>{
      dispatch(seenDetailSelectActionCreator(id));
    },
    onTrailerButtonEnter: ()=>{
      dispatch(focusTrailerButtonActionCreator());
    },
    onTrailerButtonLeave: ()=>{
      dispatch(focusTrailerButtonActionCreator());
    },
    onWatchButtonEnter: ()=>{
      dispatch(focusWatchButtonActionCreator());
    },
    onWatchButtonLeave: ()=>{
      dispatch(focusWatchButtonActionCreator());
    },
    onWatchButtonClick: (id)=>{
      dispatch(push(`/movie/${id}/play`))
    },
    onTrailerButtonClick: (id)=>{
      dispatch(push(`/movie/${id}/play_trailer`))
    }
  };
}

const DetailContent = ({title, id, genres, runtime, year,
                        synopsis, trailer, torrents,
                        images={}, onBackClick,
                        onBookmarkButtonEnter,
                        onBookmarkButtonLeave,
                        onTrailerButtonEnter,
                        onTrailerButtonLeave,
                        onTrailerButtonClick,
                        onSeenButtonEnter,
                        onSeenButtonLeave,
                        onWatchButtonEnter,
                        onWatchButtonLeave,
                        onWatchButtonClick,
                        seenButtonFocused,
                        watchButtonFocused,
                        trailerButtonFocused,
                        onSeenButtonClick,
                        onBookmarkButtonClick,
                        bookmarked,
                        seen,
})=>{
  const posterImage = images.poster;
  const backgroundImage = images.fanart;
  return (
    <DetailContainer {...{
          posterItem:<DetailPoster {...{imgSrc:posterImage}} />,
          imgSrc: backgroundImage,
          id,
          genres,
          runtime,
          title,
          year,
          synopsis,
          onBackClick,
          onBookmarkButtonEnter,
          onBookmarkButtonLeave,
          onTrailerButtonEnter,
          onTrailerButtonLeave,
          onSeenButtonEnter,
          onSeenButtonLeave,
          onWatchButtonEnter,
          onWatchButtonLeave,
          seenButtonFocused,
          watchButtonFocused,
          trailerButtonFocused,
          onSeenButtonClick,
          onBookmarkButtonClick,
          onWatchButtonClick,
          onTrailerButtonClick,
          bookmarked,
          seen
        }}
    />
  );
}
const DetailContentDecorated = connect(mapStateToProps, mapDispatchToProps)(DetailContent)

const Detail = () => {
  return (
    <MainContainer {...{items:[<Menu />, <DetailContentDecorated />]}} />
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
