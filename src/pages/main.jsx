import React from 'react';
import {connect} from 'react-redux';
import {Route, Switch, Redirect} from 'react-router-dom';
import {ConnectedRouter} from 'react-router-redux';
import Loadable from 'react-loadable';

const MyLoadable = (opts)=>{
  return Loadable(Object.assign({
    loading: ()=><span>Loading</span>,
    delay: 200,
    timeout: 10,
  }, opts));
}

const MyLoadableMoviesPage = MyLoadable({
  loader: ()=> import('./movies'),
})

const MyLoadableDetailPage = MyLoadable({
  loader: ()=> import('./detail'),
})

const MyLoadablePlayerPage = MyLoadable({
  loader: ()=> import('./player'),
})

const MyLoadableSettingsPage = MyLoadable({
  loader: ()=> import('./settings'),
})

const MyLoadableBookmarkPage = MyLoadable({
  loader: ()=> import('./bookmark'),
})

const MyLoadableInfoPage = MyLoadable({
  loader: ()=> import('./info'),
})

const mapStateToProps = state => {
  return {}
};

const Main = ({history}) => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path="/movies" component={()=><MyLoadableMoviesPage/>}/>
        <Route exact path="/shows" component={()=><MyLoadableMoviesPage/>}/>
        <Route exact path="/movie/:id" component={MyLoadableDetailPage}/>
        <Route exact path="/movie/:id/play" component={MyLoadablePlayerPage}/>
        <Route exact path="/movie/:id/play_trailer" component={MyLoadablePlayerPage}/>
        <Route exact path="/settings" component={MyLoadableSettingsPage}/>
        <Route exact path="/bookmark" component={MyLoadableBookmarkPage}/>
        <Route exact path="/info" component={MyLoadableInfoPage}/>
        <Redirect from="/" to="/movies" />
      </Switch>
    </ConnectedRouter>
  );
}

export default connect(mapStateToProps)(Main);