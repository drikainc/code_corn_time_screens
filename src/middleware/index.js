import {selectMenu} from '../actions/menu';
import {requestSearchContent, resetRow} from '../actions/movies';
import {resetPageCounterCreator} from '../actions/pages';
import {setSearchValue} from '../actions/filters';
import {requestDetailContentAction, emptyDetailContentActionCreator} from '../actions/detail';
import {replace} from 'react-router-redux';

export default store => next => action => {
  const state =  next(action);
  if(action.type == '@@router/LOCATION_CHANGE'){
    const {pathname} = action.payload;
    const path = pathname.split('/').filter((a)=>a.length!==0);
    let contentType = path[0];
    const id = path[1];
    if(path==""){
      contentType = 'movies';
    }
    if((contentType!=undefined && !id )){
      store.dispatch(selectMenu(contentType));
      store.dispatch(emptyDetailContentActionCreator());
      store.dispatch(setSearchValue('', contentType));
      switch(contentType){
        case 'movies':
          store.dispatch(resetRow(store.getState().films['pages'][contentType].lastVisitedRow, contentType));
        case 'shows':
          store.dispatch(resetRow(store.getState().films['pages'][contentType].lastVisitedRow, contentType));
          if(store.getState().films.items.length <= 0){
            store.dispatch(resetPageCounterCreator(contentType));
            store.dispatch(requestSearchContent(contentType));
          }
          break;
        case 'bookmark':
          store.dispatch(resetRow(store.getState().films['pages'][contentType].lastVisitedRow, contentType));
          break;
      }
    }else if(id && contentType!=undefined){
      store.dispatch(selectMenu(contentType+'s'));
      store.dispatch(requestDetailContentAction(id, contentType))
    }
  }
  return state;
}