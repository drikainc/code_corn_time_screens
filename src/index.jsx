import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import Main from './pages/main';
import store from './bootstrap';


window.onload = ()=>{

  const render = (Component, {store, history}) => {
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <Component history={history}/>
        </Provider>
      </AppContainer>,
      document.getElementById('content')
    );
  }

  render(Main, store());

  if (module.hot) {
    module.hot.accept('./pages/main.jsx', () => {
      const NextRootMain = require('./pages/main').default;
      render(NextRootMain);
    });
  }
}
