import {REQUEST_GET_PAGES, RESPONSE_GET_PAGES, INCREASE_PAGE_COUNTER, RESET_PAGE_COUNTER} from '../actions/pages';

const initialPages = {
  movies:{
    loading:false,
    pages:{},
    pageCounter:1,
  },
  shows:{
    loading:false,
    pages:{},
    pageCounter:1,
  }
}

export default function(state=initialPages, action){
  const {type, data, contentType} = action
  let reducedState = state;
  switch(type){
    case REQUEST_GET_PAGES:
      if(!reducedState[contentType])return state;
      reducedState[contentType].loading = true;
      break;
    case RESPONSE_GET_PAGES:
      const obj = {};
      obj[contentType]={pages:data};
      reducedState = Object.assign({}, state, obj)
      break;
    case INCREASE_PAGE_COUNTER:
      if(!reducedState[contentType])return state;
      reducedState[contentType]['pageCounter']++;
      break;
    case RESET_PAGE_COUNTER:
      if(!reducedState[contentType])return state;
      reducedState[contentType]['pageCounter'] = 1;
      break;
  }
  return Object.assign({}, reducedState);
}