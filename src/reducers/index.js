import MenuReducer from './menu';
import MoviesReducer from './movies';
import PagesReducer from './pages';
import DetailReducer from './detail';
import FilterReducer from './filters';

export default {
  menu: MenuReducer,
  films: MoviesReducer,
  pages: PagesReducer,
  detail: DetailReducer,
  filter: FilterReducer
}