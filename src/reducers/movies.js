import {
  SEE_POSTER,
  FOCUS_POSTER,
  BOOKMARK_POSTER,
  SELECT_POSTER,
  LOADED_IMAGE,
  REQUEST_SEARCH_FILMS,
  RESPONSE_SEARCH_CONTENT,
  RESET_ROW,
  RECORD_ROW,
} from '../actions/movies';
const initialFilmState = {
  items:[
    // {
    //   id:0,
    //   imgSrc:"https://image.tmdb.org/t/p/w500/kqjL17yufvn9OVLyXYpvtyrFfak.jpg",
    //   title: 'Lorem ipsum',
    //   year: 2017,
    //   seen: false,
    //   rating: 8,
    //   bookmarked: false,
    //   focused: false
    // }
  ],
  pages:{
    movies:{
      initialScrollToRow: 0,
      lastVisitedRow: 0
    },
    shows:{
      initialScrollToRow: 0,
      lastVisitedRow: 0
    },
    bookmarked:{
      initialScrollToRow: 0,
      lastVisitedRow: 0
    }
  },
  bookmarked: [],
  loading: false
};
const findIndexById = (films, posterId)=>{
  return films.findIndex(({id})=>id===posterId);
}
export default function movies(state=initialFilmState, action={}){
  const {id, type, concat, replace} = action;
  let reducedState = state;
  const index = findIndexById(state.items, id);
  const foundItem = reducedState.items[index];
  const indexBookmark = findIndexById(state.bookmarked, id);
  const foundItemBookmarked = reducedState.bookmarked[indexBookmark];
  switch(type){
    case REQUEST_SEARCH_FILMS:
        reducedState = Object.assign({}, reducedState, {loading: true})
      break;
    case RESPONSE_SEARCH_CONTENT:
        if(concat) {
          reducedState = Object.assign({}, reducedState, {
            loading: false,
            items: [...reducedState.items, ...action.films]
          })
        }else if(replace){
          reducedState = Object.assign({}, reducedState, {
            loading: false,
            items: [...action.films]
          })
        }
      break;
    case SEE_POSTER:
      reducedState.items[index] = Object.assign({}, foundItem, {seen:!foundItem.seen})
      break;
    case LOADED_IMAGE:
      reducedState.items[index] = Object.assign({}, foundItem, {loaded:true})
      break;
    case FOCUS_POSTER:
      reducedState = Object.assign({}, reducedState, {
        items:reducedState.items.slice().map((item)=>Object.assign({},item,{userFocused:false})),
      })
      reducedState.items[index] = Object.assign({}, foundItem, {userFocused:!foundItem.userFocused})
      break;
    case BOOKMARK_POSTER:
      reducedState.items[index] = Object.assign({}, foundItem, {bookmarked:!foundItem.bookmarked})
      reducedState.bookmarked = reducedState.bookmarked.slice()
      reducedState.bookmarked.push({imdb_id:foundItem.imdb_id});
      reducedState = Object.assign({}, reducedState);
      break;
    case RESET_ROW:
      reducedState.pages[action.contentType]['initialScrollToRow'] = action.row;
      break;
    case RECORD_ROW:
      reducedState.pages[action.contentType]['lastVisitedRow'] = action.row;
      break;
  }
  return reducedState;
}