import {
  SET_FILTER, SET_SORTING,
  SET_SEARCH_VALUE,
  EXPAND,
  COLLAPSE,
} from '../actions/filters';

export const CONTENT_TYPE_MOVIE = 'movies';
export const CONTENT_TYPE_SHOW = 'shows';

const initialState = {
  searchString:'',
  movies:{
    expanded: false,
    expandedSort: false,
    selectedFilter:{name:'All',value:''},
    selectedSorting:{name:'Title',value:'name'},
    genre:[
      {name:'All',value:''},
      {name:'Action',value:'action'},
      {name:'Adventure', value:'adventure'},
      {name:'Animation', value: 'animation'},
      {name:'Comedy', value: 'comedy'},
      {name:'Crime', value: 'crime'},
      {name:'Disaster', value: 'disaster'},
      {name:'Documentary', value: 'documentary'},
      {name:'Drama', value: 'drama'},
      {name:'Eastern', value: 'eastern'},
      {name:'Family', value: 'family'},
      {name:'Fan film', value: 'fan-film'},
      {name:'Fantasy', value: 'fantasy'},
      {name:'Film-Noir', value: 'film-noir'},
      {name:'History', value: 'history'},
      {name:'Holiday', value: 'holiday'},
      {name:'Horror', value: 'horror'},
      {name:'Indie', value: 'indie'},
      {name:'Music', value: 'music'},
      {name:'Musical', value: 'musical'},
      {name:'Mystery', value: 'mystery'},
      {name:'Road', value: 'road'},
      {name:'Romance', value: 'romance'},
      {name:'Sci-Fi', value:'science-fiction'},
      {name:'Short', value: 'short'},
      {name:'Sports', value: 'sports'},
      {name:'Sporting Event', value: 'sporting-event'},
      {name:'Suspense', value: 'suspense'},
      {name:'Thriller', value: 'thriller'},
      {name:'TV movie', value: 'tv-movie'},
      {name:'War', value: 'war'},
      {name:'Western', value: 'western'}
    ].map((item)=>Object.assign({},item,{selected:false})),
    sortBy:[
      {name:'Trending', value:'trending'},
      {name:'Popularity', value:'rating'},
      {name:'Last Added', value:'updated'},
      {name:'Year', value:'year'},
      {name:'Title', value:'name'},
      {name:'Rating', value:'rating'}
    ].map((item)=>Object.assign({},item,{selected:false}))
  },
  shows:{
    expanded: false,
    expandedSort: false,
    selectedFilter:{name:'All',value:''},
    selectedSorting:{name:'Title',value:'name'},
    genre:[
      {name:'All',value:''},
      {name:'Action',value:'action'},
      {name:'Adventure', value:'adventure'},
      {name:'Animation', value: 'animation'},
      {name:'Comedy', value: 'comedy'},
      {name:'Crime', value: 'crime'},
      {name:'Disaster', value: 'disaster'},
      {name:'Documentary', value: 'documentary'},
      {name:'Drama', value: 'drama'},
      {name:'Eastern', value: 'eastern'},
      {name:'Family', value: 'family'},
      {name:'Fan film', value: 'fan-film'},
      {name:'Fantasy', value: 'fantasy'},
      {name:'Film-Noir', value: 'film-noir'},
      {name:'History', value: 'history'},
      {name:'Holiday', value: 'holiday'},
      {name:'Horror', value: 'horror'},
      {name:'Indie', value: 'indie'},
      {name:'Music', value: 'music'},
      {name:'Musical', value: 'musical'},
      {name:'Mystery', value: 'mystery'},
      {name:'Road', value: 'road'},
      {name:'Romance', value: 'romance'},
      {name:'Sci-Fi', value:'science-fiction'},
      {name:'Short', value: 'short'},
      {name:'Sports', value: 'sports'},
      {name:'Sporting Event', value: 'sporting-event'},
      {name:'Suspense', value: 'suspense'},
      {name:'Thriller', value: 'thriller'},
      {name:'TV movie', value: 'tv-movie'},
      {name:'War', value: 'war'},
      {name:'Western', value: 'western'}
    ].map((item)=>Object.assign({},item,{selected:false})),
    sortBy:[
      {name:'Trending', value:'trending'},
      {name:'Popularity', value:'rating'},
      {name:'Last Added', value:'updated'},
      {name:'Year', value:'year'},
      {name:'Title', value:'name'},
      {name:'Rating', value:'rating'}
    ].map((item)=>Object.assign({},item,{selected:false}))
  }
}

export default function(state = initialState, action){
  let status = state;
  const {contentType, filter, sorting, element} = action;
  switch(action.type){
    case SET_FILTER:
      const selectedFilter = Object.assign({}, filter, {selected: true});
      status[contentType].genre = status[contentType].genre.map((config)=>Object.assign({}, config,{selected:false}));
      const index = status[contentType].genre.findIndex((item)=>item.value === selectedFilter.value)
      status[contentType].genre[index] = selectedFilter;
      status[contentType].selectedFilter = selectedFilter;
      break;
    case SET_SORTING:
      const selectedSorting = Object.assign({}, sorting, {selected: true});
      status[contentType].sortBy = status[contentType].sortBy.map((config)=>Object.assign({}, config,{selected:false}));
      const indexSorting = status[contentType].sortBy.findIndex((item)=>item.value === selectedSorting.value)
      status[contentType].sortBy[indexSorting] = selectedSorting;
      status[contentType].selectedSorting = selectedSorting;
      break;
    case SET_SEARCH_VALUE:
      status = Object.assign({}, status,{searchString:action.value});
      break;
    case COLLAPSE:
        status[contentType].expanded = false;
        status[contentType].expandedSort = false;
      break;
    case EXPAND:
      if(element==='genre'){
        status[contentType].expanded = !status[contentType].expanded
      }else if(element==='sort'){
        status[contentType].expandedSort = !status[contentType].expandedSort
      }
      break;
  }
  return status;
}