import {
  REQUEST_DETAIL_CONTENT,
  RESPONSE_DETAIL_CONTENT,
  EMPTY_DETAIL_CONTENT,
  FOCUS_BOOKMARK_BUTTON,
  FOCUS_SEEN_BUTTON,
  FOCUS_WATCH_BUTTON,
  FOCUS_TRAILER_BUTTON,
  BOOKMARK_DETAIL_SELECT,
  SEEN_DETAIL_SELECT,
} from '../actions/detail';

const initialDetailState = {
  content: {},
  id:null,
  bookmarkButtonFocused: false,
  seenButtonFocused: false,
  watchButtonFocused: false,
  trailerButtonFocused: false,
  bookmarked:false,
  seen:false,
  loading:false
};

export default function(state = initialDetailState, action){
  let reducedState = state;
  const {type, content} = action;
  switch(type){
    case BOOKMARK_DETAIL_SELECT:
      reducedState = Object.assign({},reducedState,{bookmarked:!reducedState.bookmarked});
      break;
    case SEEN_DETAIL_SELECT:
      reducedState = Object.assign({},reducedState,{seen:!reducedState.seen});
      break;
    case FOCUS_BOOKMARK_BUTTON:
      reducedState = Object.assign({},reducedState,{bookmarkButtonFocused:!reducedState.bookmarkButtonFocused});
      break;
    case FOCUS_SEEN_BUTTON:
      reducedState = Object.assign({},reducedState,{seenButtonFocused:!reducedState.seenButtonFocused});
      break;
    case FOCUS_WATCH_BUTTON:
      reducedState = Object.assign({},reducedState,{watchButtonFocused:!reducedState.watchButtonFocused});
      break;
    case FOCUS_TRAILER_BUTTON:
      reducedState = Object.assign({},reducedState,{trailerButtonFocused:!reducedState.trailerButtonFocused});
      break;
    case REQUEST_DETAIL_CONTENT:
      reducedState.loading = true;
      break;
    case EMPTY_DETAIL_CONTENT:
      reducedState = Object.assign({},{content:{}});
      break;
    case RESPONSE_DETAIL_CONTENT:
      reducedState.loading = false;
      reducedState = Object.assign(reducedState,{content});
      break;
  }
  return Object.assign({}, reducedState);
};