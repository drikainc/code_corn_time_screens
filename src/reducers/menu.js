export const MENU_LEFT = 'left';
export const MENU_RIGHT = 'right';
export const TYPE_ICON = 'icon';
export const TYPE_SEARCH = 'search';
export const TYPE_SELECT = 'select';
export const TYPE_SELECT_SORT = 'select_sort';

import {FOCUS_ITEM, SELECT_ITEM, UNSELECT_ITEM, SELECT_MENU} from '../actions/menu';

const initialMenuState = {
  items:[
    {id:0, title:'Movies', path: 'movies', pos: MENU_LEFT, focus:false, selected: false},
    {id:1, title:'Series', path: 'shows', pos: MENU_LEFT, focus: false, selected: false},
    {id:2, title:'Search', type: TYPE_SEARCH, pos: MENU_RIGHT, focus:false, selected: false},
    {id:3, title:'Settings', path: 'settings', type: TYPE_ICON, icon:'settings', pos: MENU_RIGHT, focus: false, selected: false},
    {id:4, title:'Bookmark', path: 'bookmark', type: TYPE_ICON, icon:'heart', pos: MENU_RIGHT, focus: false, selected: false},
    {id:5, title:'Info', path: 'info', type: TYPE_ICON, icon:'info', pos: MENU_RIGHT, focus:false, selected: false},
    {id:7, title:'Genre', type: TYPE_SELECT, pos: MENU_LEFT, focus:false, selected: false},
    {id:7, title:'Sort by', type: TYPE_SELECT_SORT, pos: MENU_LEFT, focus:false, selected: false},
  ]
};

function findById(items=[], searchId){
  return items.findIndex(({id})=>{return id==searchId;});
}
function findByPath(items=[], searchPath){
  return items.findIndex(({path})=>{return path==searchPath;});
}

export default function menu(state=initialMenuState, action={}){
  let reducedState = state;
  const {type, id, path} = action;
  const index = findById(reducedState.items, id);
  switch(type){
    case FOCUS_ITEM:
        reducedState.items[index] = Object.assign({}, reducedState.items[index], {focused:!reducedState.items[index].focused});
        reducedState = Object.assign({}, reducedState);
      break;
    case SELECT_MENU:
      const indexMenu = findByPath(reducedState.items, path);
      reducedState.items = reducedState.items.map((config)=>{ return Object.assign({},config,{selected:false}) });
      reducedState.items[indexMenu] = Object.assign({}, reducedState.items[indexMenu], {selected:!reducedState.items[indexMenu].selected});
      reducedState = Object.assign({}, reducedState);
      break;
    case SELECT_ITEM:
        reducedState.items = reducedState.items.map((config)=>{ return Object.assign({},config,{selected:false}) });
        reducedState.items[index] = Object.assign({}, reducedState.items[index], Object.assign({},{selected:!reducedState.items[index].selected}));
        reducedState = Object.assign({}, reducedState);
      break;
    case UNSELECT_ITEM:
      reducedState.items[index] = Object.assign({}, reducedState.items[index], {selected:false});
      reducedState = Object.assign({}, reducedState);
      break;
  }
  return reducedState;
}