import logger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { combineReducers, compose, createStore, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { routerReducer, routerMiddleware } from 'react-router-redux'
import publicReducers from './reducers';
import loadContent from './middleware'

const bootstrapStore = ()=>{
  const routes = {
   '/movies':{
    title: 'Movies'
   },
   '/content':{
    title: 'Detail',
    '/:id': {}
   },
   '/shows':{},
   '/anime':{},
   '/settings':{},
   '/bookmark':{},
   '/info':{},
  };

  const history = createHistory();
  const reduxRouterMiddleware = routerMiddleware(history);
  const store = createStore(
    combineReducers(Object.assign({},publicReducers,{router: routerReducer})),
    compose( applyMiddleware(logger, thunkMiddleware, loadContent, reduxRouterMiddleware))
  );
  window.store = store;
  return {history,store};
}

export default bootstrapStore;
