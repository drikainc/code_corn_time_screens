import fetch from 'isomorphic-fetch';

export const REQUEST_GET_PAGES='REQUEST_GET_PAGES';
export const RESPONSE_GET_PAGES='RESPONSE_GET_PAGES';
export const INCREASE_PAGE_COUNTER='INCREASE_PAGE_COUNTER';
export const RESET_PAGE_COUNTER='RESET_PAGE_COUNTER';

const requestPageActionCreator = (contentType)=>({type:REQUEST_GET_PAGES, contentType})
const responsePageActionCreator = (contentType, content)=>({type:RESPONSE_GET_PAGES, contentType, content})

export function requestPageAction(contentType){
  return (dispatch)=>{
    dispatch(requestPageActionCreator(contentType))
    fetch(`/api/${contentType}`).then((res)=>{
      return res.json();
    }).then((data)=>dispatch(responsePageActionCreator(contentType, data)))
  }
}

export function increasePageCounterCreator(contentType){
  return {type: INCREASE_PAGE_COUNTER, contentType}
}

export function resetPageCounterCreator(contentType){
  return {type: RESET_PAGE_COUNTER, contentType}
}


