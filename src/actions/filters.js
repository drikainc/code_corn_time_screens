export const SET_FILTER = 'SET_FILTER';
export const SET_SORTING = 'SET_SORTING';
export const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE';
export const EXPAND = 'EXPAND';
export const COLLAPSE = 'COLLAPSE';

export function expand(contentType, element){
  return {type: EXPAND, contentType, element};
}

export function collapse(contentType){
  return {type: COLLAPSE, contentType}
}

export function setFilter(filter, contentType){
  return {type:SET_FILTER, filter, contentType};
}

export function setSearchValue(value, contentType){
  return {type:SET_SEARCH_VALUE, value, contentType};
}

export function setSorting(sorting, contentType){
  return {type:SET_SORTING, sorting, contentType};
}
