export const SELECT_ITEM = 'SELECT_ITEM';
export const SELECT_MENU = 'SELECT_MENU';
export const UNSELECT_ITEM = 'UNSELECT_ITEM';
export const FOCUS_ITEM = 'FOCUS_ITEM';

export function selectItem(itemId){
  return {type: SELECT_ITEM, id: itemId};
}

export function selectMenu(path){
  return {type: SELECT_MENU, path};
}

export function unSelectItem(itemId){
  return {type: UNSELECT_ITEM, id: itemId};
}
export function focusItem(itemId){
  return {type: FOCUS_ITEM, id: itemId};
}
