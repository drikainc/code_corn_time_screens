export const REQUEST_DETAIL_CONTENT = 'REQUEST_DETAIL_CONTENT';
export const RESPONSE_DETAIL_CONTENT = 'RESPONSE_DETAIL_CONTENT';
export const EMPTY_DETAIL_CONTENT = 'EMPTY_DETAIL_CONTENT';
export const FOCUS_BOOKMARK_BUTTON = 'FOCUS_BOOKMARK_BUTTON';
export const FOCUS_SEEN_BUTTON = 'FOCUS_SEEN_BUTTON';
export const FOCUS_WATCH_BUTTON = 'FOCUS_WATCH_BUTTON';
export const FOCUS_TRAILER_BUTTON = 'FOCUS_TRAILER_BUTTON';
//move BOOKMARK_DETAIL to a centralized service
export const BOOKMARK_DETAIL_SELECT = 'BOOKMARK_DETAIL_SELECT';
export const SEEN_DETAIL_SELECT = 'SEEN_DETAIL_SELECT';

import fetch from 'isomorphic-fetch';

export function bookmarkDetailSelectActionCreator(id){
  return {type: BOOKMARK_DETAIL_SELECT, id};
}

export function seenDetailSelectActionCreator(id){
  return {type: SEEN_DETAIL_SELECT,id};
}

export function focusBookmarkButtonActionCreator(){
  return {type: FOCUS_BOOKMARK_BUTTON};
}

export function focusSeenButtonActionCreator(){
  return {type: FOCUS_SEEN_BUTTON};
}

export function focusWatchButtonActionCreator(){
  return {type: FOCUS_WATCH_BUTTON};
}

export function focusTrailerButtonActionCreator(){
  return {type: FOCUS_TRAILER_BUTTON};
}

function requestDetailContentActionCreator(id, contentType){
  return {type: REQUEST_DETAIL_CONTENT, id, contentType}
}

function responseDetailContentActionCreator(id, contentType, content){
  return {type: RESPONSE_DETAIL_CONTENT, id, contentType, content}
}

export function emptyDetailContentActionCreator(){
  return {type: EMPTY_DETAIL_CONTENT}
}

export function requestDetailContentAction(id, type){
  return (dispatch)=>{
    dispatch(requestDetailContentActionCreator(id, type))
    fetch(`/api/${type}/${id}`)
      .then((res)=>res.json())
      .then((data)=>{
        dispatch(responseDetailContentActionCreator(id, type, data))
      });
  }
}
