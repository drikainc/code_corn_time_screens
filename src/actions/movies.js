import fetch from 'isomorphic-fetch';
import qs from 'qs';
import {increasePageCounterCreator} from '../actions/pages';
import {resetRow} from '../actions/movies';
import {setSearchValue} from '../actions/filters';

export const SEE_POSTER = 'SEEN_POSTER';
export const BOOKMARK_POSTER = 'BOOKMARKED_POSTER';
export const FOCUS_POSTER = 'FOCUS_POSTER';
export const SELECT_POSTER = 'SELECT_POSTER';
export const REQUEST_SEARCH_FILMS = 'REQUEST_SEARCH_FILMS';
export const REQUEST_SEARCH_CONTENT = 'REQUEST_SEARCH_CONTENT';
export const RESPONSE_SEARCH_CONTENT = 'RESPONSE_SEARCH_CONTENT';
export const LOADED_IMAGE = 'LOADED_IMAGE';
export const RECORD_ROW = 'RECORD_ROW';
export const RESET_ROW = 'RESET_ROW';


export function seeItem(itemId){
  return {type: SEE_POSTER, id: itemId};
}

export function recordRow(row, contentType){
  return {type: RECORD_ROW, row, contentType};
}

export function resetRow(row, contentType){
  return {type: RESET_ROW, row, contentType}
}

export function bookmarkItem(itemId){
  return {type: BOOKMARK_POSTER, id: itemId};
}

export function loadedImage(itemId){
  return {type: LOADED_IMAGE, id: itemId};
}

export function selectItem(itemId){
  return {type: SELECT_POSTER, id: itemId};
}

export function focusItem(itemId){
  return {type: FOCUS_POSTER, id: itemId};
}

export function requestSearchAction(){
  return {type: REQUEST_SEARCH_FILMS}
}

export function responseSearchAction(films, concat=false, replace){
  return {type: RESPONSE_SEARCH_CONTENT, films, concat, replace}
}

export function requestSearchFilms(filters={movies:{}}, page=1, type='movies', concat=false){
  return (dispatch)=>{
    dispatch(requestSearchAction());
    if(!filters.movies){
      filters = {movies:{selectedFilter:{value:''}, selectedSorting:{value:''}},searchString:''}
    }
    const genre = filters.movies.selectedFilter.value;
    const sorting = filters.movies.selectedSorting.value;
    const order = -1
    const keywords = filters.searchString;
    const obj = {genre, sorting, order, keywords};
    const searchWord = qs.stringify(obj);
    fetch(`/api/${type}/${page}?${searchWord}`).then((res)=>{
      return res.json();
    }).then((films)=>{
      return films.map((config)=>Object.assign({}, config, {id:config.imdb_id}))
    }).then((films)=>{
      dispatch(increasePageCounterCreator(type));
      dispatch(responseSearchAction(films, concat, searchWord.length > 1))
      dispatch(resetRow(-1, type));
    });
  }
}

export function requestSearchSeries(filters=[], type='shows', page=1, concat=false){
  return (dispatch)=>{
    dispatch(requestSearchAction());
    fetch(`/api/${type}/${page}`).then((res)=>{
      return res.json();
    }).then((films)=>{
      return films.map((config)=>Object.assign({}, config, {id:config.imdb_id}))
    }).then((films)=>{
      dispatch(increasePageCounterCreator(type));
      dispatch(responseSearchAction(films, concat))
    });
  }
}

function requestSearchContentAction(){
  return {type: REQUEST_SEARCH_CONTENT};
}

export function requestSearchContent(path, filter=[]){
  return (dispatch)=>{
    dispatch(requestSearchContentAction);
    switch(path){
      case 'movies':
        dispatch(requestSearchFilms(filter))
        break;
      case 'shows':
        dispatch(requestSearchSeries(filter))
        break;
    }
  }
}
