import React from 'react';
import {connect} from 'react-redux';
import InfiniteScroll from 'react-infinite-scroll-component';
import Container from 'code_corn_time_components/src/components/poster/container';
import Poster from './poster';
import NoConnectedPoster from 'code_corn_time_components/src/components/poster/poster';
import {requestSearchFilms, recordRow, resetRow} from '../../actions/movies'
import {AutoSizer, Grid} from 'react-virtualized';
import {throttle} from 'lodash';
import posterStyle from 'code_corn_time_components/src/components/poster/poster.css';
import {intersectionWith, uniqWith} from 'lodash';

const mapStateToProps = ({films, shows, pages = {movies: {pageCounter: 1}}, router, filter}) => {
  const {items, loading} = films;
  const posterItems = items.map((filmConfiguration) => {
    return {item: Poster, config: filmConfiguration}
  })
  const scrollToRow = films.lastVisitedRow;
  const type = router.location.pathname.split('/')[1];
  const contentType = type;
  const initialScrollToRow = films['pages'][contentType]['initialScrollToRow'];
  let counter = 1
  try {
    counter = pages[type].pageCounter
  } catch (e) {
  }
  return {
    items: posterItems,
    loading,
    counter,
    filter,
    scrollToRow,
    initialScrollToRow,
    contentType
  };
}

const mapStateToBookmarkProps = ({films}) => {
  const items = films.items.map((filmConfiguration) => {
    return {item: Poster, config: Object.assign({}, filmConfiguration)}
  });
  const intersection = intersectionWith(items, films.bookmarked, (firstVal, secondVal) => {
    const result = (firstVal.config.imdb_id == secondVal.imdb_id);
    return result;
  });
  const itemsT = uniqWith(intersection.slice(), (prev, next) => prev.config.imdb_id == next.config.imdb_id)
  console.log('itemsLength', itemsT.length);
  return {
    items: itemsT
  }
}

const mapDispatchBookmarksToProps = () => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadNext: (counter, location, filter = []) => {
      return ((counter, location, filter) => {
        dispatch(requestSearchFilms(filter, counter, location, true));
      }).bind(this, counter, location, filter);
    },
    recordRow: (row, contentType) => {
      dispatch(recordRow(row, contentType));
    },
    resetRow: (row, contentType) => {
      dispatch(resetRow(row, contentType));
    }
  };
}

const cellRenderer = (props, items, longScroll) => {
  const {style} = props;
  const aggregatedStyle = Object.assign({}, style, {
    display: 'flex',
    justifyContent: 'center'
  });
  const {rowIndex, columnIndex} = props;
  if (items.length <= 0)return null;
  const arrPos = (rowIndex * props.parent.props.columnCount) + columnIndex;
  const itemConfig = items[arrPos];
  // console.log(itemConfig);
  if (!itemConfig) return null;
  const {item, config} = itemConfig;
  const Item = item;
  const isStable = (!props.isScrolling || !longScroll) && props.isVisible;
  return (
    <div style={aggregatedStyle}>{
      <Item {...Object.assign({}, config, {
        stable: isStable,
        focus: !props.isScrolling
      })}/>
    }</div>
  );
}

const onScroll = (counter, location, filter, loadNext) => {
  const deb = throttle((counter, location, filter) => {
    loadNext(counter, location, filter)()
  }, 1000, {leading: true});
  deb(counter, location, filter);
  deb.cancel();
}

const GridComp = ({
                    applyRowScroll = function(){},
                    scrollToRow,
                    initialScrollToRow,
                    contentType,
                    resetRow = function(){},
                    recordRow = function () {},
                    width, height, loading = true, items, counter = 0, location, filter = [], loadNext = function () {
  }
                  }) => {

  let columnCount = Math.max(1, Math.ceil(width / parseInt(posterStyle.width)));
  if (width % parseInt(posterStyle.width) <= 0) {
    let columnCount = Math.max(1, Math.floor(width / parseInt(posterStyle.width)));
  }
  let rowCount = Math.max(1, Math.ceil(items.length / columnCount));
  if (items.length % columnCount <= 0) {
    rowCount = Math.max(1, Math.floor(items.length / columnCount));
  }
  let prevScrollTop = 0;
  let longScroll = false;
  let throttleScroll = null
  let throttleRecordRow = null;
  const posterHeight = parseInt(posterStyle.height);
  window.resizeTo(window.innerWidth, window.innerHeight);
  return (<Grid
    cellRenderer={(props) => cellRenderer(props, items, longScroll)}
    columnWidth={width / columnCount}
    rowCount={rowCount}
    columnCount={columnCount}
    rowHeight={posterHeight}
    height={height}
    width={width}
    overscanRowCount={1}
    scrollToRow={initialScrollToRow}
    scrollTop={0}
    onSectionRendered={({rowStopIndex}) => {
        recordRow(rowStopIndex, contentType);
        window['row']= rowStopIndex;
    }}
    onScroll={({scrollTop, clientHeight}) => {
      throttleScroll = throttleScroll || throttle((scrollTopVal, prev) => {
          if (Math.abs(prev - scrollTopVal) > posterHeight * 0.3) {
            longScroll = true;
          } else {
            longScroll = false;
          }
          prevScrollTop = scrollTopVal;
        }, 300, {leading: true});
      throttleScroll(scrollTop, prevScrollTop);
      if (items.length <= 0)return;
      const height = (posterHeight * rowCount - scrollTop - clientHeight)
      if (!loading && height <= parseInt(posterStyle.height) * Math.floor(rowCount / 5)) {
        onScroll(counter, location, filter, loadNext)
      }

    }}
    scrollingResetTimeInterval={800}
  />);
}

const ContainerLoader2 = (props) => {
  const Item = (
    <AutoSizer>
      {({width, height}) => <GridComp {...Object.assign({}, props, {
        width,
        height
      })}/>}
    </AutoSizer>
  );
  return <Container items={Item}/>
}

const mergeProps = (mapProps, dispatchProps, ownProps) => {
  return Object.assign({}, mapProps, dispatchProps, ownProps);
}

const ConnectedComp = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ContainerLoader2);

const ConnectedCompBookmarks = connect(mapStateToBookmarkProps, mapDispatchBookmarksToProps, mergeProps)(ContainerLoader2);

export {ConnectedCompBookmarks};

export default ConnectedComp;

