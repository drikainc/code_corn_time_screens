import React from 'react';
import {push} from 'react-router-redux';
import {connect} from 'react-redux';
import Poster from 'code_corn_time_components/src/components/poster/poster';

import {selectItem, seeItem, bookmarkItem, focusItem, loadedImage} from '../../actions/movies';

const mapStateToProps = ()=>{
  return {};
}

const mapDispatchToProps = (dispatch)=>{
  return {
    onSelect: (id)=>{
      dispatch(selectItem(id));
      dispatch(push(`/movie/${id}`))
    },
    onSee: (id)=>{
      dispatch(seeItem(id))
    },
    onPosterLoaded: (id)=>{
      dispatch(loadedImage(id));
    },
    onBookmark: (id)=>{
      dispatch(bookmarkItem(id));
    },
    onFocus: (id)=>{
      dispatch(focusItem(id))
    }
  };
}

const mapMergeToProps = (stateToProps, dispatchToProps, ownProps)=>{
  const { imdb_id, images, focus } = ownProps;
  const mappedOwnProps = { id: imdb_id, rating:0, imgSrc: images.poster, focus};
  return Object.assign({}, stateToProps, dispatchToProps, mappedOwnProps, ownProps);
}

const posterWrapper = (data)=>{
  const {id} = data;
  return (
    <Poster {...data}/>
  );
}

export default connect(mapStateToProps, mapDispatchToProps, mapMergeToProps)(posterWrapper);

