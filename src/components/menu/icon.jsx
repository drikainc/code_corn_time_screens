import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {FaCog, FaHeart, FaInfoCircle} from 'react-icons/lib/fa';
import Icon from 'code_corn_time_components/src/components/menu/icon';

import {focusItem, selectItem} from '../../actions/menu';

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    onClick: (id, path)=>{
      // dispatch(push(path))
    },
    onEnter: (id)=>{
      dispatch(focusItem(id));
    },
    onLeave: (id)=>{
      dispatch(focusItem(id));
    }
  }
};

const mergeProps = (stateProps, dispatchProps, ownProps) =>{
  let icon = undefined;
  switch(ownProps.icon){
    case 'settings':
      icon = <FaCog/>;
      break;
    case 'heart':
      icon = <FaHeart/>;
      break;
    case 'info':
      icon = <FaInfoCircle/>;
      break;
  }
  const props = Object.assign({},{items:[icon]});
  return Object.assign({}, ownProps, stateProps, dispatchProps, props);
}

const Component = (props)=>{
  const {path} = props;
  return (
    <Link to={path}>
      <Icon {...props}/>
    </Link>
  );
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Component)
