import React from 'react';
import {connect} from 'react-redux';
import Menu from 'code_corn_time_components/src/components/menu/menu';
import Item from './item';
import Icon from './icon';
import Search from './search';
import Select from './select';
import SelectSort from './sort_select';

import {MENU_LEFT, MENU_RIGHT, TYPE_ICON, TYPE_SEARCH, TYPE_SELECT, TYPE_SELECT_SORT} from '../../reducers/menu';

const mapStateToProps = ({menu, filter}) => {
  const {items} = menu;
  const {movies, shows} = filter;
  return {
    itemsLeft:items.slice().filter((item)=>item.pos===MENU_LEFT).map((props)=>{
      const {id, title, type,focused,selected, path} = props;
      let Component = undefined;
      let config = undefined;
      switch(type){
        case TYPE_SELECT:
          config ={id, key:id, text:title, selected, value: 'TT'}
          Component = Select;
          break;
        case TYPE_SELECT_SORT:
          config ={id, key:id, text:title, selected, value: 'TT'}
          Component = SelectSort;
          break;
        default:
          config ={id, key:id, name:title, focused, selected, path}
          Component = Item;
          break;
      }
      return <Component {...config} />
    }),
    itemsRight:items.slice().filter((item)=>item.pos===MENU_RIGHT).map((props)=>{
      const {id, title,focused,selected, icon, type, path} = props;
      let Component = undefined;
      switch(type){
        case TYPE_ICON:
          Component = Icon;
          break;
        case TYPE_SEARCH:
          Component = Search;
          break;
        case TYPE_SELECT:
          Component = Select;
          break;
      }
      return <Component {...{id, key:id, name:title, focused, selected, icon, path:`/${path}`}} />
    }),
  };
};

const mapDispatchToProps = dispatch => {
  return {
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
