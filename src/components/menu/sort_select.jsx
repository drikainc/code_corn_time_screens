import React from 'react';
import {connect} from 'react-redux';
import Select from 'code_corn_time_components/src/components/select/select';

import {expand, collapse, setSorting} from '../../actions/filters';
import {requestSearchFilms} from '../../actions/movies';

const span=({name, value})=>{
  return <span>{name}</span>
}

const mapFunc = (config, key)=>{
  return {element:span, config, key}
};

const mapStateToProps = ({filter}) => {
  return ({
    expanded: filter.movies.expandedSort,
    options: filter.movies.sortBy.map(mapFunc),
    value: filter.movies.selectedSorting.name,
    filter:filter
  });
}

const mapDispatchToProps = dispatch => {
  return {
    onSelectClick: (id)=>{
      dispatch(collapse('movies'));
      dispatch(expand('movies','sort'));
    },
    onClickElement: (configuration, filter)=>{
      dispatch(collapse('movies'));
      dispatch(setSorting(configuration, 'movies'));
      dispatch(requestSearchFilms(filter))
    }
  }
};

const mergeProps = (stateProps, dispatchProps, ownProps)=>{
  return Object.assign({}, ownProps, stateProps, dispatchProps);
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Select)
