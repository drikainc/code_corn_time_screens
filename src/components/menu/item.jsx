import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import Item from 'code_corn_time_components/src/components/menu/item';

import {focusItem, selectItem} from '../../actions/menu';

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    onClick: (id)=>{
      // dispatch(selectItem(id));
      // dispatch(push(path))
    },
    onEnter: (id)=>{
      dispatch(focusItem(id));
    },
    onLeave: (id)=>{
      dispatch(focusItem(id));
    }
  }
};

const Component = (props)=>{
  const {path} = props;
  return (
    <Link to={`/${path}`}>
      <Item {...props}/>
    </Link>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Component)
