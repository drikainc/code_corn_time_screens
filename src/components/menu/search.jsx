import React from 'react';
import {connect} from 'react-redux';
import Search from 'code_corn_time_components/src/components/search/search';

import {focusItem, selectItem, unSelectItem} from '../../actions/menu';
import {setSearchValue} from '../../actions/filters';
import {requestSearchFilms} from '../../actions/movies'

const mapStateToProps = ({filter}) => {
  return {filter};
};

const mapDispatchToProps = dispatch => {
  return {
    onClick: (id)=>{
      dispatch(selectItem(id));
    },
    onSubmit: (value,id, filter)=>{
      dispatch(unSelectItem(id));
      const filterItem = Object.assign({}, filter);
      filterItem.searchString = value;
      dispatch(requestSearchFilms(filterItem));
    },
    onEnter: (id)=>{
      dispatch(focusItem(id));
    },
    onLeave: (id)=>{
      dispatch(focusItem(id));
    },
    onBlur: (id)=>{
      dispatch(unSelectItem(id));
    }
  }
};

const mergeProps = (stateProps, dispatchProps, ownProps)=>{
  return Object.assign({}, ownProps, stateProps, dispatchProps, {expanded: ownProps.selected});
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Search)
